import { Controller } from '@nestjs/common';
import { Ctx, EventPattern, MessagePattern, NatsContext, Payload } from '@nestjs/microservices';

const posts = [
    {
        id: 1,
        title: 'title 1'
    },
    {
        id: 2,
        title: 'title 2'
    },
    {
        id: 3,
        title: 'title 3'
    },
]

@Controller('posts')
export class PostController {
    @MessagePattern({ cmd: 'get_all_posts' }) // MessagePattern: xử lý bất đồng bộ và có return
    // @EventPattern({ cmd: 'get_all_post' }) // EventPattern: không xử lý bất đồng bộ, không return
    // accumulate(data: { select: number }): { id: number, title: string }[] {
    accumulate(@Payload() data: any, @Ctx() context: NatsContext): { id: number, title: string }[] {
        console.log("🚀 ~ context:", context) // chỉ log được khi đi cùng Payload
        // args: [
        //     JsonSocket {
        //         socket: [Socket],
        //         isClosed: false,
        //         contentLength: null,
        //         buffer: '',
        //         stringDecoder: [StringDecoder],
        //         delimiter: '#'
        //     },
        //     '{"cmd":"get_all_posts"}'
        // ]
        console.log("🚀 ~ data:", data)
        return posts.slice(0, data.select ?? posts.length)
    }

    @EventPattern('hello')
    handleHello(data: string) {
        console.log("🚀 ~ data:", data)
    }
}
