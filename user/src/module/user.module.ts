import { Module } from '@nestjs/common';
import { ClientProxyFactory, ClientsModule, Transport } from '@nestjs/microservices';
import { UserController } from 'src/controller/user.controller';
import { UserService } from 'src/service/user.service';

@Module({
    imports: [
        // ClientsModule.register([
        //     {
        //         name: 'POST_SERVICE',
        //         transport: Transport.TCP,
        //         options: {
        //             host: 'localhost',
        //             port: 5000,
        //         }
        //     }
        // ])
        ClientsModule.register([
            {
                name: 'POST_SERVICE',
                transport: Transport.REDIS,
                options: {
                    host: 'localhost',
                    port: 6379,
                }
            }
        ])
    ],
    providers: [
        UserService,
        // {
        //     provide: 'POST_SERVICE',
        //     useFactory: () => {
        //         ClientProxyFactory.create({
        //             transport: Transport.TCP,
        //             options: {
        //                 host: 'localhost',
        //                 port: 5000,
        //             }
        //         })
        //     },
        // }
    ],
    controllers: [UserController],
})
export class UserModule { }
