import { Controller, Get, Inject, Res } from '@nestjs/common';
import { Client, ClientProxy, Transport } from '@nestjs/microservices';
import { Response } from 'express';

@Controller('users')
export class UserController {
    constructor(
        @Inject('POST_SERVICE') private readonly postService: ClientProxy,
    ) {
    }

    // @Client({
    //     transport: Transport.TCP, options: {
    //         port: 5000,
    //         host: 'localhost'
    //     }
    // })
    private client: ClientProxy;

    @Get('')
    async getAllPosts(@Res() response: Response) {

        // this.client.send(
        //     { cmd: 'get_all_posts' },
        //     { select: 2 })
        //     .subscribe(res => {
        //         response.send({
        //             message: "Success",
        //             res
        //         })
        //     })
        this.postService.send(
            { cmd: 'get_all_posts' },
            { select: 2 }).subscribe(res => {
                response.send({
                    message: "Success",
                    res
                })
            })
    }

    @Get('hello')
    async handleHello() {
        this.client.emit('hello', 'send from user service')
    }
}
